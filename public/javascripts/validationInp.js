//$('.ui.dropdown').dropdown();
$('.ui.checkbox').checkbox();

/**
 * function used to implement semantic UI form validation(jQuery/javascript)
 */
$('.ui.form').form({
  bedrooms: {
    identifier: 'bedrooms',
    rules: [{
      type: 'empty',
      prompt: 'Please enter the number of bedrooms field',
    },],
  },
  rent: {
    identifier: 'rent',
    rules: [{
      type: 'empty',
      prompt: 'Please enter a rent amount field',
    },],
  },
  area: {
    identifier: 'area',
    rules: [{
      type: 'empty',
      prompt: 'Please fill in the area field',
    },],
  },
  bathrooms: {
    identifier: 'bathrooms',
    rules: [{
      type: 'empty',
      prompt: 'Please fill in the amount of bathrooms field',
    },],
  },
  eircode: {
    identifier: 'eircode',
    rules: [{
      type: 'empty',
      prompt: 'Please fill in the eircode field',
    },],
  },
  type: {
    identifier: 'type',
    rules: [{
      type: 'empty',
      prompt: 'Please select a residence type',
    },],
  },
});
