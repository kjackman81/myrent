$('.ui.dropdown').dropdown();

/**
 * function used to implement semantic form validation, onSuccess executes two
 * more functions
 */
$('.ui.form.tenant').form({
	fields : {
		newTenantResidence : {
			identifier : 'newTenantResidence',
			rules : [ {
				type : 'empty',
				prompt : 'Please a tenants name',
			}, ],
		},
	},

	onSuccess : function(event, fields) {
		submitForm3();
		event.preventDefault();
	},
});
