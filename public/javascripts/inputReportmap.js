/**
 * method used to create an object to define the properties for the map(radius,
 * center, zoom, etc.......), listens out and captures the new position of the
 * marker after 'dragend'
 */
function initialize() {
	const
	latlng = new google.maps.LatLng(52.254427, -7.185281);

	const
	mapOptions = {
		center : new google.maps.LatLng(latlng.lat(), latlng.lng()),
		mapTypeId : google.maps.MapTypeId.SATELLITE,
		zoom : 16,
	};


	const
	mapDiv = document.getElementById('map-canvas');
	const
	map = new google.maps.Map(mapDiv, mapOptions);
	mapDiv.style.width = '100%';
	mapDiv.style.height = '100%';

	const
	marker = new google.maps.Marker({
		map : map,
		draggable : true,
		position : latlng,
		title : 'Drag and drop on your property!',
		animation : google.maps.Animation.BOUNCE,

	});

	// To add the marker to the map, call setMap();
	marker.setMap(map);

	// marker listener populates hidden fields ondragend
	google.maps.event.addListener(marker, 'dragend', function() {
		const
		latLng = marker.getPosition();
		const
		latlong = latLng.lat().toString().substring(0, 10) + ','
				+ latLng.lng().toString().substring(0, 10);
		// publish lat long in geolocation control in html page
		$('#geolocation').val(latlong);
		// update the new marker position
		map.setCenter(latLng);
	});

}

/**
 * method that uses the built in DOM listener method to implement the map on the
 * page it will execute the initialize() function on window load
 */
google.maps.event.addDomListener(window, 'load', initialize);

(function() {
	console.log('running ok');
}());
