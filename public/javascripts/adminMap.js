/**
 * methods used to add jQuery formating(javascript under the hood) on the
 * dropdowns
 */
$('.ui.dropdown.landlordNames').dropdown('clear');
$('.ui.dropdown.tenantNames').dropdown('clear');

/**
 * variables to store data
 */
let latlong = [];
var map;
const markers = [];

/**
 * method used to create an object (mapProp) to define the properties for the
 * map(radius, center, zoom, etc.......)
 */
function initialize()
{
  retrieveMarkerLocations();

  var center = new google.maps.LatLng(53.347298, -6.268344);
  var initRadius = 10000;
  var mapProp = {
    center: center,
    zoom: 7,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  };

  var mapDiv = document.getElementById('map_canvas');

  mapDiv.style.width = '100%';
  mapDiv.style.height = '500px';

  map = new google.maps.Map(mapDiv, mapProp);  
}

/**
 * Use ajax call to get users and their geolocations pass returned array marker
 * locations to callback method Here is the format in which marker data stored
 * geoObj[0] is descripion. geoObj[1] is latitude geoObj[2] is longitude We use
 * geoObj[0] in the infoWindow. Click marker to reveal description.
 */
function retrieveMarkerLocations()
{
  $(function () {
    $.get('/Administrations/getCordinates', function (data) {
      $.each(data, function (index, geoObj) {
        console.log(geoObj[0] + ' ' + geoObj[1] + ' ' + geoObj[2] + ' ' + geoObj[3] + ' ' + geoObj[4] + ' ' + geoObj[5]);
      });

      callback(data);
    });
  });
}

/**
 * we've got the marker location from data in ajax call we now put data into an
 * array the format is 'firstName, xx.xxxx, yy.yyyyy' -> (firstName, lat, lng)
 * then invoke 'fitBounds' to render the markers, centre map and create
 * infoWindow to display firstName
 */
function callback(data)
{
  removeMarkers()
  latlng = data; // store the array of data in a global for later use
  fitBounds(latlng); // then invoke fitBounds to zoom and display markers
  setInfoWindowListener(latlng);
}

/**
 * creates and positions markers sets zoom so that all markers visible
 */
function fitBounds(latlngStr)
{
  var image = '/public/images/marker.png';
  const bounds = new google.maps.LatLngBounds();
  for (let i = 0; i < latlngStr.length; i++)
  {
    marker = new google.maps.Marker({
      position: getLatLng(latlngStr[i]),
     // animation: google.maps.Animation.DROP,
      icon: image,
      map: map,

    });
    markers[i] = marker;
    bounds.extend(marker.position);// extends map to fit marker positions
  }

  map.fitBounds(bounds);// add the bounds to the map
}



/**
 * method that retrieves the array of marker data, listens for the 'click',
 * displays the details of that marker(tenant, eircode, landlord)
 * 
 * @param latlngStr
 */
function setInfoWindowListener(latlngStr)
{
  const infowindow = new google.maps.InfoWindow();
  for (let i = 0; i < latlng.length; i++)
  {
    /* respond to click on marker by displaying infowindow text */
    const marker = markers[i];
    google.maps.event.addListener(marker, 'click', (function (marker, i) {
      return function () {
        infowindow.setContent('tenant: ' + latlngStr[i][3] + '<br />' + 'eircode: ' + latlngStr[i][4] + '<br />' + 'landlord: ' + latlngStr[i][5]);
        infowindow.open(map, marker);
      };
    })(marker, i));
  }
}

/**
 * A helper function to convert the latlng string to individual numbers and
 * thence to a google.maps.LatLng object
 * 
 * @param str
 *            str is list of strings : username, lat, lon str[0] is description
 *            str[1] is latitude str[2] is longitude
 * 
 * @param The
 *            object 'str' holding an individual marker data set
 * @return A google.maps.LatLng object containing the marker coordinates.
 */

function getLatLng(str)
{

  const lat = Number(str[1]);
  const lon = Number(str[2]);
  return new google.maps.LatLng(lat, lon);
}

/**
 * method used to remove markers from the map in order to re-initialize them
 * without duplicates
 */
function removeMarkers()
{
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
}

/**
 * method that uses the built in DOM listener method to implement the map on the
 * page it will execute the initialize() function on window load
 */
google.maps.event.addDomListener(window, 'load', initialize);
