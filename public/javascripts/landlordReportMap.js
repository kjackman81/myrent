var circle;

/**
 * function that defines the coordinates for the hidden fields in the form data
 * for the circle search area 
 */
function requestReport() {
	var center = circle.getCenter();
	var latcenter = center.lat().toString();
	var lngcenter = center.lng().toString();
	var radius = circle.getRadius().toString();
	$('#radius').val(radius);
	$('#latcenter').val(latcenter);
	$('#lngcenter').val(lngcenter);
}

/**
 * method used to create an object (mapProp) to define the properties for the
 * map(radius, center, zoom, etc.......), uses the circle object to define the
 * search area and sets it to the map
 */
function initialize() {
	var center = new google.maps.LatLng(53.347298, -6.268344);
	var initRadius = 10000;
	var mapProp = {
		center : center,
		zoom : 7,
		mapTypeId : google.maps.MapTypeId.ROADMAP,
	};
	var mapDiv = document.getElementById('map_canvas');
	var map = new google.maps.Map(mapDiv, mapProp);
	mapDiv.style.width = '100%';
	mapDiv.style.height = '500px';

	circle = new google.maps.Circle({
		center : center,
		radius : initRadius,
		strokeColor : '#0000FF',
		strokeOpacity : 0.4,
		strokeWeight : 1,
		fillColor : '#0000FF',
		fillOpacity : 0.4,
		draggable : true,
	});
	circle.setEditable(true);// allows varying radius be dragging anchor
	// point
	circle.setMap(map);
}

/**
 * method that uses the built in DOM listener method to implement the map on the
 * page it will execute the initialize() function on window load
 */
google.maps.event.addDomListener(window, 'load', initialize);
