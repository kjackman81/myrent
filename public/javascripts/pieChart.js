let chartArray = [];

/**
 * immediately-invoked function, Ajax call to the following URL, to retrieve the
 * array of landlord percentages, invokes a callback function
 */
(function ()
{
  $(function () {
    $.get('/Administrations/getPercentages', function (data) {
      $.each(data, function (index, geoObj) {
        console.log(geoObj[0] + ' ' + geoObj[1]);
      });

      callback(data);
    });
  });
}());

/**
 * function that defines the chart object's properties(text, theme, data
 * etc....) and renders the chart to the view
 */
function chart() {

  var chart = new CanvasJS.Chart('chartContainer',
  {
    title: {
      text: 'Landlord rent rolls as % income all protfolios',
    },
    animationEnabled: true,
    legend: {
      verticalAlign: 'center',
      horizontalAlign: 'right',
      fontSize: 10,
      fontFamily: 'Helvetica',
    },
    theme: 'theme2',
    data: [
    {
      type: 'pie',
      indexLabelFontFamily: 'Garamond',
      indexLabelFontSize: 10,
      indexLabel: '{label} {y}%',
      startAngle: -20,
      showInLegend: true,
      toolTipContent: '{legendText} {y}%',
      dataPoints: chartArray,

      /*
		 * [ { y: chartArray[0][0], legendText: chartArray[0][1], label:
		 * chartArray[0][1]}, { y: chartArray[1][0], legendText:
		 * chartArray[1][1], label: chartArray[1][1]}, { y: chartArray[2][0],
		 * legendText: chartArray[2][1], label: chartArray[2][1]}, { y:
		 * chartArray[3][0], legendText: chartArray[3][1], label:
		 * chartArray[3][1]}, { y: chartArray[4][0], legendText:
		 * chartArray[4][1], label: chartArray[4][1]}, ]
		 */
    },
    ],
  });
  chart.render();
}

/**
 * function used to format a array data in order to satisfy the criteria of the
 * chart() data[dataPoints: array]
 * 
 * @param data
 */
function callback(data)
{

  for (let i = 0; i < data.length; i++)
   {
    chartArray[i] = { y: data[i][0], legendText: data[i][1], label: data[i][1] };
    console.log('y: ' + data[i][0] + ' ' + 'legendText: ' +  data[i][1] + ' ' + 'label: ' +  data[i][1]);
    chart();
  }
}
