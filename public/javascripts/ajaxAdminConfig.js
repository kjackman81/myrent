$('.ui.dropdown.landlordNames').dropdown('clear');
$('.ui.dropdown.tenantNames').dropdown('clear');


/**
 * function used for form validation of dropdown, onSuccess it fires of a
 * function to submitForm0
 */
$('.ui.form.landlord').form({
  fields: {
    landlordId: {
      identifier: 'landlordId',
      rules: [{
        type: 'empty',
        prompt: 'Please a landlords name',
      },],
    },
  },

  onSuccess: function (event, fields) {
      submitForm0();
      event.preventDefault();
    },
});

/**
 * function used to send an Ajax request with form data to the following URL,
 * when a successful response is returned it executes the remaining two function
 */
function submitForm0() {
  let formData = $('.ui.form.landlord').serialize();
  console.log('formData: ', formData);

  $.ajax({
    type: 'POST',
    url: '/admin/deleteLandlord',

    data: formData,
    success: function (response) {

      console.log('landlord server response :' + response);
      dropdownlandlordClear(response);
      retrieveMarkerLocations();
    },
  });
}

/**
 * function used to remove a element or name from a dropdown list if the
 * comparison criteria is met
 * 
 * @param name
 */
function dropdownlandlordClear(name) {
  let $obj = $('.landlordsNames');

  console.log('$obj: ', $obj);

  for (let i = 0; i < $obj.length; i += 1) {

    if ($obj[i].getAttribute('data-value').localeCompare(name) == 0) {

      $obj[i].remove();
      $('.ui.dropdown.landlordNames').dropdown('clear');

      break;
    }
  }
}

/**
 * function used for form validation of dropdown, onSuccess it fires of a
 * function to submitForm1
 */
$('.ui.form.tenant').form({
  fields: {
    tenantId: {
      identifier: 'tenantId',
      rules: [{
        type: 'empty',
        prompt: 'Please a landlords name',
      },],
    },
  },

  onSuccess: function (event, fields) {
      submitForm1();
      event.preventDefault();
    },
});

/**
 * function used to send an Ajax request with form data to the following URL,
 * when a successful response is returned it executes the remaining two function
 */
function submitForm1() {
  let formData = $('.ui.form.tenant').serialize();
  console.log('formData: ', formData);

  $.ajax({
    type: 'POST',
    url: '/admin/deleteTenant',

    data: formData,
    success: function (response) {

      console.log('tenant server response :' + response);
      dropdownTenantClear(response);
      retrieveMarkerLocations();
    },
  });
}

/**
 * function used to remove a element or name from a dropdown list if the
 * comparison criteria is met
 * 
 * @param name
 */
function dropdownTenantClear(name) {
  let $obj = $('.tenantsNames');

  for (option in $obj)
  console.log('$obj: ', $obj[option]);

  for (let i = 0; i < $obj.length; i += 1) {

    if ($obj[i].getAttribute('data-value').localeCompare(name) == 0) {

      $obj[i].remove();
      $('.ui.dropdown.tenantNames').dropdown('clear');

      break;
    }
  }
}

