//$('.ui.dropdown').dropdown();
$('.ui.checkbox').checkbox();

/**
 * function used to implement semantic UI form validation(jQuery/javascript)
 */
$('.ui.form').form({
  firstName: {
    identifier: 'firstName',
    rules: [{
      type: 'empty',
      prompt: 'Please enter your first name',
    },],
  },
  lastName: {
    identifier: 'lastName',
    rules: [{
      type: 'empty',
      prompt: 'Please select last name',
    },],
  },
  email: {
    identifier: 'email',
    rules: [{
      type: 'empty',
      prompt: 'Please enter a valid address',
    },],
  },
  address: {
    identifier: 'address',
    rules: [{
      type: 'empty',
      prompt: 'Please enter a email',
    },],
  },
  password: {
    identifier: 'password',
    rules: [{
      type: 'empty',
      prompt: 'Please enter a password',
    }, {
      type: 'minLength[6]',
      prompt: 'Your password must be at least 6 characters',
    },],
  },
  agree: {
    identifier: 'agree',
    rules: [{
      type: 'checked',
      prompt: 'You must agree to the terms and conditions',
    },],
  },
});

/**
 * function used to insure confirmation of agreement 
 */
function agreeed() {
  var terms = document.getElementById('agreeTerms').checked;

  if (terms === false)
    alert('you must agree to our terms and conditions');
}
