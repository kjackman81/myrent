/**
 * function that terminates the residency by using and Ajax request to the URL
 * to generate a response in order to update the DOM dynamically, set the label
 * to '0000' and executes two more functions
 */
function terminateRes()
{

  $.ajax({
    type: 'GET',
    url: '/tenant/terminateResidency',
    success: function (response) {

      console.log('terminate Residence server response :' + response);
      $('.ui.label.eircode').text('0000');
      dropdownResAdd(response);
      retrieveMarkerLocations();
    },
  });

}

/**
 * function used to update the dropdown in the DOM dynamically, creates a new
 * OPTION element and adds it to the list of options in the selection dropdown,
 * using the response parameter as both text and value
 * 
 * @param response
 */
function dropdownResAdd(response) {

  var x = document.getElementById('selectResOption');
  console.log('select x: ', x);
  var option = document.createElement('option');
  option.text = response;
  option.value = response;
  x.add(option);

	 console.log('select y: ', x);

}
/**
 * when validation is complete this Ajax function sends the form data to the
 * URL, after execution the response is used to update the label on the page and
 * two more functions are executed
 */
function submitForm3() {
  let formData = $('.ui.form.tenant').serialize();
  console.log('formData: ', formData);

  $.ajax({
    type: 'POST',
    url: '/tenant/addNewTenant',

    data: formData,
    success: function (response) {

      console.log('Residence server response :', response);
      $('.ui.label.eircode').text(response);
      dropdownResClear(response);
      retrieveMarkerLocations();
    },
  });
}

/**
 * when a tenant is added to a residence the DOM has to be updated, this
 * function removes the eircode or residence from the list now that it is no
 * longer vacant
 * 
 * @param name
 */
function dropdownResClear(name) {

  let $obj =  document.getElementById('selectResOption');
  console.log('$obj from dropdownRes clesr before :', $obj);

  for (let i = 0; i < $obj.length; i += 1) {

    if ($obj[i].getAttribute('value').localeCompare(name) == 0) {

      console.log(' obj[i] remove response :', name);

      $obj[i].remove();
      $('.ui.dropdown.tenants').dropdown('clear');
      console.log('$obj from dropdownRes clesr after :', $obj);
      break;
    }}
}

