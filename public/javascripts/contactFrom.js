/**
 * functions built into semantic to implement jQuery(javascript)functionality on
 * the dropdowns
 */
$('.ui.dropdown.firstName').dropdown('clear');
$('.ui.dropdown.lastName').dropdown('clear');
$('.ui.dropdown.email').dropdown('clear');
