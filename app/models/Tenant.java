package models;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import play.db.jpa.Model;

@Entity
public class Tenant extends Model {

	/**
	 * variables used to store landlords data
	 */
	public String firstName;
	public String lastName;
	public String email;
	public String password;
	public String address;

	@OneToOne
	public Landlord landlord;

	/**
	 * constructor used to instantiate a tenant object with passed in parameters
	 * to initialize variables.
	 * 
	 * @param Landlord
	 *            landlord
	 * @param String
	 *            email
	 * @param String
	 *            firstName
	 * @param String
	 *            lastName
	 * @param String
	 *            address
	 * @param String
	 *            password
	 */
	public Tenant(Landlord landlord, String email, String firstName,
			String lastName, String address, String password) {
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.password = password;
		this.landlord = landlord;
	}

	/**
	 * method used to verify the password by comparing the passed in variable
	 * against the existing value
	 * 
	 * @param String
	 *            password
	 * 
	 * @return boolean
	 * 
	 */
	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}

	/**
	 * method used to find the first email matching the passed in parameter
	 * 
	 * @param String
	 *            email
	 * 
	 * @return Tenant 
	 */
	public static Tenant findByEmail(String email) {
		return find("email", email).first();
	}

	/**
	 * overrides the built in toString method
	 * 
	 * @return String first + last names
	 */
	@Override
	public String toString() {
		return this.firstName + " " + this.lastName;
	}
}
