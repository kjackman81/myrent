package models;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import play.db.jpa.Model;
import utils.LatLng;

@Entity
public class Residence extends Model {

	/**
	 * variables used to store data on a residence object
	 */
	public String location;
	public int rent;
	public int bedrooms;
	public int area;
	public int bathrooms;
	public String type;
	public String eircode;
	public Date date;

	@ManyToOne
	public Landlord landlord;

	@OneToOne
	public Tenant tenant;

	/**
	 * constructor used to instantiate a residence object with specified
	 * parameters to initialize variables
	 * 
	 * @param Landlord
	 *            landlord
	 * @param Tenant
	 *            tenant
	 * @param String
	 *            geolocation
	 * @param String
	 *            eircode
	 * @param int
	 *            rent
	 * @param int
	 *            bedrooms
	 * @param int
	 *            area
	 * @param int
	 *            bathrooms
	 * @param String
	 *            type
	 */
	public Residence(Landlord landlord, Tenant tenant, String geolocation,
			String eircode, int rent, int bedrooms, int area, int bathrooms,
			String type) {

		this.location = geolocation;
		this.rent = rent;
		this.bedrooms = bedrooms;
		this.area = area;
		this.bathrooms = bathrooms;
		this.type = type;
		this.landlord = landlord;
		this.eircode = eircode;
		this.tenant = tenant;
		this.date = new Date();

	}

	/**
	 * method used to convert a String location into a LatLng for the use in
	 * google mapping of markers
	 * 
	 * @return LatLng
	 */
	public LatLng getGeolocation() {

		return LatLng.toLatLng(location);
	}

	/**
	 * method used to find the first eircode matching the passed in parameter
	 * 
	 * @param String
	 *            eircode
	 * 
	 * @return Residence 
	 */
	public static Residence findByEircode(String eircode) {
		return find("eircode", eircode).first();
	}

}
