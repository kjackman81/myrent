package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.Model;

@Entity
public class Message extends Model {

	/**
	 * variables used to store data on message objects
	 */
	public String messageText;
	public String email;

	@ManyToOne
	public Landlord from;
	public Date date;

	/**
	 * constructor used to instantiate a message object with passed in
	 * parameters to initialize variables
	 * 
	 * 
	 * @param Landlord
	 *            from
	 * @param String
	 *            message
	 * @param String
	 *            email
	 */
	public Message(Landlord from, String message, String email) {

		this.messageText = message;
		this.from = from;
		this.email = email;
		this.date = new Date();

	}

}
