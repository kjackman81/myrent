package models;

import javax.persistence.Entity;

import play.db.jpa.Model;

@Entity
public class Administrator extends Model {

	/**
	 * variables used to store admin data
	 */
	public String email;
	public String password;
	public String firstName;
	public String lastName;

	/**
	 * constructor used to instantiate object
	 */
	public Administrator() {

	}

	/**
	 * method used to verify the password by comparing the passed in variable
	 * against the existing value
	 * 
	 * @param String
	 *            password
	 * 
	 * @return boolean
	 * 
	 */
	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}

	/**
	 * method used to find the first email matching the passed in parameter
	 * 
	 * @param String
	 *            email
	 * 
	 * @return Administrator
	 */
	public static Administrator findByEmail(String email) {
		return find("email", email).first();
	}

	/**
	 * overrides the built in toString method
	 * 
	 * @return String first + last names
	 */
	@Override
	public String toString() {
		return this.firstName + " " + this.lastName;
	}

}
