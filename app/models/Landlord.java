package models;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import play.db.jpa.Model;
import play.db.jpa.Blob;

@Entity
public class Landlord extends Model {

	/**
	 * variables used to store landlords data
	 */
	public String email;
	public String firstName;
	public String lastName;
	public String password;
	public String address;

	/**
	 * constructor used to instantiate a landlord object with passed in
	 * parameters to initialize variables.
	 * 
	 * @param String
	 *            email
	 * @param String
	 *            firstName
	 * @param String
	 *            lastName
	 * @param String
	 *            address
	 * @param String
	 *            password
	 */

	public Landlord(String email, String firstName, String lastName,
			String address, String password) {
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.address = address;
	}

	/**
	 * method used to verify the password by comparing the passed in variable
	 * against the existing value
	 * 
	 * @param String
	 *            password
	 * 
	 * @return boolean
	 * 
	 */
	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}

	/**
	 * method used to find the first email matching the passed in parameter
	 * 
	 * @param String
	 *            email
	 * 
	 * @return Landlord 
	 */
	public static Landlord findByEmail(String email) {
		return find("email", email).first();
	}

	/**
	 * overrides the built in toString method
	 * 
	 * @return String first + last names
	 */
	@Override
	public String toString() {
		return this.firstName + " " + this.lastName;
	}

}
