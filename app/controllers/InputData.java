package controllers;

import play.*;
import play.mvc.*;
import play.mvc.Scope.Session;
import java.util.*;

import models.*;

public class InputData extends Controller {

	/**
	 * method used to render the input data page with the current logged in
	 * landlord object
	 * 
	 */
	public static void inputData() {
		Landlord luser = Landlords.getCurrentLandLord();

		if (luser == null) {
			Logger.info("myRent input data page: Unable to getCurrentUser");
			Landlords.logIn();
		}

		Logger.info("input data page rendering " + luser.firstName);
		render(luser);
	}

	/**
	 * method used to facilitate the registration of a new residence with
	 * parameters and storing it in the Residence database
	 * 
	 * @param String
	 *            geolocation
	 * @param String
	 *            eircode
	 * @param int
	 *            rent
	 * @param int
	 *            bedrooms
	 * @param int
	 *            area
	 * @param int
	 *            bathrooms
	 * @param String
	 *            type
	 */
	public static void dataEntry(String geolocation, String eircode, int rent,
			int bedrooms, int area, int bathrooms, String type) {
		Landlord user = Landlords.getCurrentLandLord();

		Logger.info(geolocation + " " + rent + " " + bedrooms + " " + type);
		Residence residence = new Residence(user, null, geolocation, eircode,
				rent, bedrooms, area, bathrooms, type);
		Logger.info("new residence created");
		residence.save();
		InputData.inputData();
	}

	/**
	 * method used to remove a residence from the Residence database using the
	 * passed in formal parameter to find the residence by long Id and using the
	 * delete()
	 * 
	 * @param long
	 *            deleteResidenceId
	 */
	public static void deleteResidence(long deleteResidenceId) {
		Logger.info("residenceID:  " + deleteResidenceId);

		Residence residence = Residence.findById(deleteResidenceId);
		if (residence == null) {
			Logger.info("could not find residence:  " + residence);
			Landlords.landlordConfig();
		} else {
			Logger.info("residence deleted:  " + residence.id);
			residence.delete();
			Landlords.landlordConfig();
		}

	}

	/**
	 * method used to render a view which allows the user to edit a selected
	 * residence found by the residence Id
	 *
	 * @param long
	 *            editResidenceId
	 */
	public static void editResidence(long editResidenceId) {
		Landlord lUser = Landlords.getCurrentLandLord();

		Residence residence = Residence.findById(editResidenceId);
		if (residence == null) {
			Logger.info("residence cannot be found:  " + residence);
			Landlords.landlordConfig();
		}

		render(lUser, residence);

	}

	/**
	 * method used to amend the selected residence, uses the formal parameters
	 * to achieve this by finding the residence by Id and reassigning the rent
	 * attribute
	 * 
	 * @param long
	 *            id
	 * @param int
	 *            rent
	 */
	public static void ammendResidence(long id, int rent) {
		Residence residence = Residence.findById(id);

		// residence.eircode = (eircode.isEmpty())? residence.eircode : eircode;
		Logger.info("rent to be changed to:   " + rent);

		residence.rent = (rent > 0) ? rent : residence.rent;

		residence.save();

		Landlords.landlordConfig();
	}

}
