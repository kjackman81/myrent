package controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import models.Administrator;
import models.Landlord;
import models.Message;
import models.Residence;
import models.Tenant;
import play.Logger;
import play.mvc.Controller;

public class Administrations extends Controller {

	/**
	 * Gson object instantiated, for the purpose of rendering data back to the
	 * script for DOM manipulation
	 */
	static Gson gson = new GsonBuilder().create();

	/**
	 * method renders the login page
	 */
	public static void logIn() {
		render();
	}

	/**
	 * method used to logout, clears the session data and renders the admin
	 * login page
	 */
	public static void logOut() {
		session.clear();
		logIn();
	}

	/**
	 * method used to render the admin page, populated with the current
	 * administrator, the list of current tenants and the list of current
	 * landlords
	 */
	public static void adminConfig() {
		Administrator admin = getCurrentAdmin();

		List<Tenant> tenants = Tenant.findAll();
		List<Landlord> landlords = Landlord.findAll();

		Logger.info("just landed on admin config page");

		render(admin, tenants, landlords);
	}

	/**
	 * method used to authenticate weather the passed in credentials are correct
	 * before allowing access to the application, checks both email and password
	 * for validation, if correct access is approved and the key and value are
	 * put into the current session, if incorrect renders the login page
	 * 
	 * @param String
	 *            email
	 * @param String
	 *            password
	 */
	public static void authenticate(String email, String password) {
		Logger.info("email: " + email + " password: " + password);

		Administrator admin = Administrator.findByEmail(email);

		if (admin != null && admin.checkPassword(password)) {
			session.put("logged_in_adminId", admin.id);
			Logger.info("authentication approved for: " + admin.firstName + " "
					+ admin.lastName);
			adminConfig();
		}

		Logger.info("unable to validate credentials" + email + " " + password);
		logIn();
	}

	/**
	 * method used to retrieve(get)the current logged in administrator, from the
	 * current session
	 * 
	 * @return Administrator logged_in_admin
	 */
	public static Administrator getCurrentAdmin() {
		String userId = session.get("logged_in_adminId");
		if (userId == null || userId.isEmpty()) {
			return null;

		}

		Administrator logged_in_admin = Administrator
				.findById(Long.parseLong(userId));
		Logger.info(
				"the logged in administrator is: " + logged_in_admin.firstName);
		return logged_in_admin;

	}

	/**
	 * method used to create and store a new landlord in the database with the
	 * passed in formal parameters as actual parameter for the landlord object
	 * 
	 * @param String
	 *            firstName
	 * @param String
	 *            lastName
	 * @param String
	 *            email
	 * @param String
	 *            address
	 * @param String
	 *            password
	 */
	public static void registerLord(String firstName, String lastName,
			String email, String address, String password) {

		Landlord lUser = new Landlord(email, firstName, lastName, address,
				password);
		lUser.save();

		Logger.info("admin registered details: " + firstName + " " + lastName
				+ " " + email + " " + address + " " + password);

		adminConfig();
	}

	/**
	 * method used to create and store a new tenant in the database with the
	 * passed in formal parameters as actual parameter for the tenant object
	 * 
	 * @param String
	 *            firstName
	 * @param String
	 *            lastName
	 * @param String
	 *            email
	 * @param String
	 *            address
	 * @param String
	 *            password
	 */
	public static void registerTenant(String firstName, String lastName,
			String email, String address, String password) {

		Tenant tUser = new Tenant(null, email, firstName, lastName, address,
				password);
		tUser.save();

		Logger.info("admin registered details: " + firstName + " " + lastName
				+ " " + email + " " + address + " " + password);

		adminConfig();
	}

	/**
	 * method used to remove a tenant from the Tenant database, uses the passed
	 * in parameter tenantId to find the unique tenant, deletes this from the
	 * database as well as using the gson.toJson method to return the name of
	 * the tenant to the script to allow the ajax to update specific parts of
	 * the page dynamically(dropdowns marker locations etc......)
	 * 
	 * @param tenantId
	 */
	public static void deleteTenant(long tenantId) {
		List<Residence> residences = Residence.findAll();
		Tenant tenant = Tenant.findById(tenantId);
		Logger.info("tenant name:  " + tenant.firstName);

		for (Residence residence : residences) {
			if (residence.tenant != null && residence.tenant.equals(tenant)) {
				residence.tenant = null;
				residence.save();
			}

		}
		String response = gson.toJson(tenant.firstName);
		tenant.delete();
		renderJSON(response);

		// adminConfig();
	}

	/**
	 * method used to remove a landlord from the Landlord database, uses the
	 * passed in parameter landlordId to find the unique landlord, deletes this
	 * from the database as well as using the gson.toJson method to return the
	 * name of the landlord to the script to allow the ajax to update specific
	 * parts of the page dynamically(dropdowns marker locations etc......)
	 * 
	 * @param landlordId
	 */
	public static void deleteLandlord(long landlordId) {

		List<Residence> residences = Residence.findAll();
		List<Message> messages = Message.findAll();

		List<Tenant> tenants = Tenant.findAll();

		Landlord landlord = Landlord.findById(landlordId);
		// Logger.info("landlord name: " + landlord.firstName);

		for (Residence residence : residences) {
			if (residence.landlord.equals(landlord)) {
				residence.delete();
			}
		}

		for (Tenant tenant : tenants) {
			if (tenant.landlord != null && tenant.landlord.equals(landlord)) {
				tenant.landlord = null;
				tenant.save();
			}
		}

		for (Message message : messages) {
			if (message.from.equals(landlord)) {
				message.delete();
			}
		}

		Logger.info("landlord removed: " + landlord.firstName);

		String response = gson.toJson(landlord.firstName);

		landlord.delete();
		renderJSON(response);

	}

	/**
	 * method used to render a jsonArray list of lists of Strings, each list
	 * numbered by its key flag, specifying details about a individual residence
	 * including its geolocation coordinates(using the getGeolocation()), id,
	 * tenant, etc....
	 */
	public static void getCordinates() {
		int flag = 0;
		List<Residence> allResidences = Residence.findAll();
		List<List<String>> jsonArray = new ArrayList<List<String>>();

		for (Residence res : allResidences) {

			String name = res.landlord.firstName;
			String id = Long.toString(res.id);
			String lonG = Double.toString(res.getGeolocation().getLongitude());
			String lat = Double.toString(res.getGeolocation().getLatitude());
			String tenantName = (res.tenant == null)
					? "avaiable"
					: res.tenant.firstName;
			String eircode = res.eircode;

			jsonArray.add(flag,
					Arrays.asList(id, lat, lonG, tenantName, eircode, name));
			flag++;
		}

		renderJSON(jsonArray);

	}

	/**
	 * method used to calculate the total rent for each landlord(uses the
	 * getTotalRent()), and uses this to calculate what percentage(uses the
	 * percentage()) of the total overall rent it is, renders this data in a
	 * jsonArray to the script to manipulate the charting facility
	 */
	public static void getPercentages() {
		int totalRent = 0;
		int flag = 0;

		List<Residence> allResidences = Residence.findAll();
		List<Landlord> allLandlords = Landlord.findAll();
		List<List<String>> jsonArray = new ArrayList<List<String>>();

		for (Residence res : allResidences) {
			totalRent += res.rent;
		}

		for (Landlord landlord : allLandlords) {

			int myRent = (getTotalRent(landlord));

			String myRentString = percentage(myRent, totalRent);

			jsonArray.add(flag,
					Arrays.asList(myRentString, landlord.firstName));

			flag++;

		}
		renderJSON(jsonArray);

	}

	/**
	 * method used to render a jsonArray list of lists of Strings, each list
	 * numbered by its key flag, specifying details about a individual residence
	 * where the Tenant parameter is equal to null, including its geolocation
	 * coordinates(using the getGeolocation()), id, tenant, etc....
	 */
	public static void getVacantCordinates() {
		int flag = 0;
		List<Residence> allResidences = Residence.findAll();
		List<List<String>> jsonArray = new ArrayList<List<String>>();

		for (Residence res : allResidences) {

			if (res.tenant == null) {
				String bathrooms = Integer.toString(res.bathrooms);
				String bedrooms = Integer.toString(res.bedrooms);
				String rent = Integer.toString(res.rent);
				String name = res.landlord.firstName;
				String id = Long.toString(res.id);
				String lonG = Double
						.toString(res.getGeolocation().getLongitude());
				String lat = Double
						.toString(res.getGeolocation().getLatitude());
				String tenantName = (res.tenant == null)// used to specify null
														// occupancy
						? "avaiable"
						: res.tenant.firstName;
				String eircode = res.eircode;
				Date date = res.date;
				String sDate = date.toString();
				String area = Integer.toString(res.area);

				jsonArray.add(flag, Arrays.asList(id, lat, lonG, tenantName,
						eircode, name, sDate, rent, bedrooms, bathrooms, area));
				flag++;
			}
		}
		renderJSON(jsonArray);

	}

	/**
	 * method used to get the total amount of rent received by the passed in
	 * landlord
	 * 
	 * @param Landlord
	 *            landlord
	 * 
	 * @return myTotalRent
	 */
	public static int getTotalRent(Landlord landlord) {
		int myTotalRent = 0;

		List<Residence> allResidences = Residence.findAll();

		for (Residence res : allResidences) {
			if (res.landlord.equals(landlord)) {
				myTotalRent += res.rent;
			}
		}
		Logger.info("myTotalRent is:  " + myTotalRent);
		return myTotalRent;

	}

	/**
	 * method used to calculate what percentage of the total overall rent(int
	 * totalRent) a passed in value is(int rent)
	 * 
	 * @param int
	 *            rent
	 * @param int
	 *            totalRent
	 * 
	 * @return String
	 */
	public static String percentage(int rent, int totalRent) {
		Logger.info("Rent is:  " + rent);

		return Integer.toString((rent * 100) / totalRent);
	}

	/**
	 * method used to render the admin report chart page with all residences
	 * data for display
	 */
	public static void adminChart() {
		List<Residence> residences = Residence.findAll();
		render("/Report/adminChart.html", residences);
	}

}
