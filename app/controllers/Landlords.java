package controllers;

import play.*;
import play.mvc.*;
import play.mvc.Scope.Session;
import java.util.*;

import models.*;

public class Landlords extends Controller {

	/**
	 * method that renders the landlord sign up view, if the current user is the
	 * administrator it renders them to the page also, this is to differentiate
	 * between the normal sign up view and the admin version
	 */
	public static void signUp() {

		Logger.info("landed at landlord signUp page");

		Administrator admin = Administrations.getCurrentAdmin();
		if (admin != null) {
			render(admin);
		}
		render();
	}

	/**
	 * method that render the log in view
	 */
	public static void logIn() {
		Logger.info("landed at landlord logIn page");
		render();
	}

	/**
	 * method to render an edit view facility, with the logged in users details
	 * available to amend
	 */
	public static void editData() {
		Landlord user = getCurrentLandLord();
		render(user);
	}

	/**
	 * method to log out the current user, clears the session and renders the
	 * log in page
	 */
	public static void logOut() {
		session.clear();
		logIn();
	}

	/**
	 * method used to render the landlord configuration view, accompanied by the
	 * current landlord user object and a list of there residences
	 */
	public static void landlordConfig() {
		Landlord lUser = getCurrentLandLord();
		List<Residence> allResidences = Residence.findAll();
		List<Residence> myResidences = new ArrayList<Residence>();

		for (Residence res : allResidences) {
			if (res.landlord.equals(lUser)) {
				myResidences.add(res);
			}
		}

		render(lUser, myResidences);
	}

	/**
	 * method used to create and store a new landlord object in the Landlord
	 * database, retrieves the data from the sign up page form to satisfy the
	 * landlord constructors parameters and saves
	 * 
	 * @param String
	 *            firstName
	 * @param String
	 *            lastName
	 * @param String
	 *            email
	 * @param String
	 *            address
	 * @param String
	 *            password
	 */
	public static void register(String firstName, String lastName, String email,
			String address, String password) {

		Landlord lUser = new Landlord(email, firstName, lastName, address,
				password);
		lUser.save();

		Logger.info(firstName + " " + lastName + " " + email + " " + address
				+ " " + password);

		logIn();
	}

	/**
	 * method used to authenticate weather the passed in credentials are correct
	 * before allowing access to the application, checks both email and password
	 * for validation, if correct access is approved and the key and value are
	 * put into the current session, if incorrect renders the login page
	 * 
	 * @param String
	 *            email
	 * @param String
	 *            password
	 */
	public static void authenticate(String email, String password) {

		Logger.info(
				"Attempting to authenticate with " + email + ":" + password);

		Landlord lUser = Landlord.findByEmail(email);

		if ((lUser != null) && (lUser.checkPassword(password) == true)) {

			session.put("logged_in_landlordId", lUser.id);

			Logger.info("You are good to GO!!!:  " + lUser.firstName + " "
					+ lUser.lastName);

			landlordConfig();
		} else {
			Logger.info("Authentication failed");
			Welcome.index();
		}
	}

	/**
	 * method used to retrieve(get)the current logged in landlord, from the
	 * current session
	 * 
	 * @return Landlord logged_in_landlord
	 */
	public static Landlord getCurrentLandLord() {
		String userId = session.get("logged_in_landlordId");
		if (userId == null || userId.isEmpty()) {
			return null;

		}

		Landlord logged_in_landlord = Landlord.findById(Long.parseLong(userId));
		Logger.info("the logged in user is: " + logged_in_landlord.firstName);
		return logged_in_landlord;

	}

	/**
	 * method used to implement any changes to the current logged in users
	 * details, data retrieved from edit data view form, any change to the users
	 * details are recognized and updated with confirmation of password
	 * 
	 * @param String
	 *            firstName
	 * @param String
	 *            lastName
	 * @param String
	 *            email
	 * @param String
	 *            address
	 * @param String
	 *            password
	 */
	public static void amend(String firstName, String lastName, String email,
			String address, String password) {

		Logger.info("landords details to be saved: " + " " + firstName + " "
				+ lastName + " " + email + " " + address + " " + password);

		Landlord lUser = getCurrentLandLord();

		if (lUser.checkPassword(password)) {

			lUser.firstName = (firstName.isEmpty() || firstName == null)
					? lUser.firstName
					: firstName;
			lUser.lastName = (lastName.isEmpty() || lastName == null)
					? lUser.lastName
					: lastName;
			lUser.email = (email.isEmpty() || email == null)
					? lUser.email
					: email;
			lUser.address = (address.isEmpty() || address == null)
					? lUser.address
					: address;
			// lUser.password = (password.isEmpty() || password == null) ?
			// currentUser.password : password;

			Logger.info("You just got amended   :" + lUser.firstName + " "
					+ lUser.lastName);
			lUser.save();
			InputData.inputData();
		}

		else {

			Logger.info("Authentication failed: creditentilas are incorrect");
			logIn();
		}

	}

}
