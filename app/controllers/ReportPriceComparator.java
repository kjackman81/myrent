package controllers;

import java.util.Comparator;

import models.Residence;

public class ReportPriceComparator implements Comparator<Residence> {

	/**
	 * method overridden to satisfy the implementation of the Comparator
	 * interface, used to compare the rent values of two residences and return a
	 * int value(-1, 0, 1) depending on a lower, even or higher amount
	 * comparison, to be used in the built in Collections.sort(array,
	 * comparator) method
	 * 
	 * @return int -1 || 0 || 1
	 */
	@Override
	public int compare(Residence r0, Residence r1) {

		return Integer.compare(r0.rent, r1.rent);
	}

}
