package controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import models.Administrator;
import models.Landlord;
import models.Residence;
import models.Tenant;
import play.Logger;
import play.mvc.Controller;

public class Tenants extends Controller {

	/**
	 * Gson object instantiated, for the purpose of rendering data back to the
	 * script for DOM manipulation
	 */
	static Gson gson = new GsonBuilder().create();

	/**
	 * method used to render the tenant log in view
	 */
	public static void logIn() {
		Logger.info("landed at tenant logIn page");
		render();
	}

	/**
	 * method that renders the tenant sign up view, if the current user is the
	 * administrator it renders them to the page also, this is to differentiate
	 * between the normal sign up view and the admin version
	 */
	public static void signUp() {
		Logger.info("landed at tenant signUp page");
		Administrator admin = Administrations.getCurrentAdmin();
		if (admin != null) {
			render(admin);
		}
		render();
	}

	/**
	 * method to log out the current user, clears the session and renders the
	 * log in page
	 */
	public static void logOut() {
		session.clear();
		logIn();
	}

	/**
	 * method used to create and store a new tenant object in the Tenant
	 * database, retrieves the data from the sign up page form to satisfy the
	 * tenant constructors parameters and saves
	 * 
	 * @param String
	 *            firstName
	 * @param String
	 *            lastName
	 * @param String
	 *            email
	 * @param String
	 *            address
	 * @param String
	 *            password
	 */
	public static void register(String firstName, String lastName, String email,
			String address, String password) {

		Tenant tUser = new Tenant(null, email, firstName, lastName, address,
				password);
		tUser.save();

		Logger.info(firstName + " " + lastName + " " + email + " " + address
				+ " " + password);

		logIn();
	}

	/**
	 * method used to authenticate weather the passed in credentials are correct
	 * before allowing access to the application, checks both email and password
	 * for validation, if correct access is approved and the key and value are
	 * put into the current session, if incorrect renders the login page
	 * 
	 * @param String
	 *            email
	 * @param String
	 *            password
	 */
	public static void authenticate(String email, String password) {

		Logger.info(
				"Attempting to authenticate with " + email + ":" + password);

		Tenant tUser = Tenant.findByEmail(email);

		if ((tUser != null) && (tUser.checkPassword(password) == true)) {

			session.put("logged_in_tenantId", tUser.id);

			Logger.info("You are good to GO!!!:  " + tUser.firstName + " "
					+ tUser.lastName);

			tenantConfig();
		} else {
			Logger.info("Authentication failed");
			Welcome.index();
		}
	}

	/**
	 * method used to render a tenant configuration view, including the current
	 * logged in users residence eircode(if 'any' else '0000')and a list of all
	 * vacant residences
	 */
	public static void tenantConfig() {
		String myEircode;

		Tenant tUser = getCurrentTenant();
		List<Residence> allResidences = Residence.findAll();
		List<Residence> vacantResidences = new ArrayList<Residence>();

		Residence myResidence = null;

		for (Residence res : allResidences) {
			if (res.tenant != null) {
				myResidence = (res.tenant.equals(tUser)) ? res : myResidence;
			}
		}

		for (Residence res : allResidences) {
			if (res.tenant == null) {
				vacantResidences.add(res);
			}
		}

		// myEircode = (myResidence.eircode != null ||
		// !myResidence.eircode.isEmpty()) ? myResidence.eircode : "0000" ;

		if (myResidence == null) {
			myEircode = "0000";
		} else {
			myEircode = myResidence.eircode;
		}

		render(myEircode, vacantResidences);
	}

	/**
	 * method used to terminate a residence agreement by changing the residence
	 * tenant field to null, the tenants landlord field to null and saving, the
	 * gson.toJson() response is also related back to the script in order to
	 * manipulation the DOM, adding and removing vacant and non vacant
	 * residences from list and views
	 */
	public static void terminateResidency() {
		String response = null;

		Tenant tUser = getCurrentTenant();
		List<Residence> allResidences = Residence.findAll();

		for (Residence residence : allResidences) {
			if (residence.tenant != null && residence.tenant.equals(tUser)) {

				Logger.info("residence agreement terminated : "
						+ residence.eircode);

				response = gson.toJson(residence.eircode);
				residence.tenant = null;
				tUser.landlord = null;
				tUser.save();
				residence.save();

			}
		}

		renderJSON(response);

		// tenantConfig();

	}

	/**
	 * method used to add a tenant to a residence, by using the formal parameter
	 * newTenantResidence to find the residence(findByEircode()), checks to see
	 * that it is vacant, checks to make sure the current user is not a tenant
	 * already, assigns the residences tenant field to the current tenant user,
	 * assigns the current tenants landlord field to the residences landlord
	 * field and saves, the gson.toJson() response is also related back to the
	 * script in order to manipulation the DOM, adding and removing tenants and
	 * non tenants from list and views
	 * 
	 * @param String
	 *            newTenantResidence
	 */
	public static void addNewTenant(String newTenantResidence) {

		String response = null;

		Logger.info("residence to update: " + newTenantResidence);

		Residence residence = Residence.findByEircode(newTenantResidence);

		if (residence.tenant != null) {
			response = gson.toJson(null);
			renderJSON(response);
		}
		Tenant tUser = getCurrentTenant();

		Logger.info("residence tenant is: " + tUser.firstName);

		if (tUser.landlord != null) {

			Logger.info(
					"cant complete it seems you are already a tenant, you need to cancel your first agreement");

		} else {

			residence.tenant = tUser;
			tUser.landlord = residence.landlord;
			tUser.save();
			residence.save();
			response = gson.toJson(residence.eircode);
			renderJSON(response);
		}

	}

	/**
	 * method used to render a jsonArray list of lists of Strings, each list
	 * numbered by its key flag, specifying details about a individual residence
	 * where the Tenant parameter is equal to null, including its geolocation
	 * coordinates(using the getGeolocation()), id, tenant, etc....
	 */
	public static void getVacantCordinates() {
		int flag = 0;
		List<Residence> allResidences = Residence.findAll();
		List<List<String>> jsonArray = new ArrayList<List<String>>();

		for (Residence res : allResidences) {

			if (res.tenant == null) {
				String name = res.landlord.firstName;
				String id = Long.toString(res.id);
				String lonG = Double
						.toString(res.getGeolocation().getLongitude());
				String lat = Double
						.toString(res.getGeolocation().getLatitude());
				String tenantName = (res.tenant == null)
						? "avaiable"
						: res.tenant.firstName;
				String eircode = res.eircode;

				jsonArray.add(flag, Arrays.asList(id, lat, lonG, tenantName,
						eircode, name));
				flag++;
			}
		}
		renderJSON(jsonArray);

	}

	/**
	 * method used to retrieve(get)the current logged in tenant, from the
	 * current session
	 * 
	 * @return Tenant logged_in_tenant
	 */
	public static Tenant getCurrentTenant() {
		String userId = session.get("logged_in_tenantId");
		if (userId == null || userId.isEmpty()) {
			return null;

		}

		Tenant logged_in_tenant = Tenant.findById(Long.parseLong(userId));
		Logger.info("the logged in tenant is: " + logged_in_tenant.firstName);
		return logged_in_tenant;

	}

}