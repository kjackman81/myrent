package controllers;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import models.Landlord;
import models.Residence;
import models.Tenant;
import play.Logger;
import play.mvc.Before;
import play.mvc.Controller;
import utils.Circle;
import utils.Geodistance;
import utils.LatLng;

public class Report extends Controller {

	/**
	 * method used to render the landlord report view
	 */

	public static void reportLandlord() {

		Logger.info("Landed on report page");
		render();
	}

	/**
	 * method to be executed before each action call in the controller. Checks
	 * that a user has logged in. If no user logged in the user is presented
	 * with the welcom screen.
	 */
	@Before
	static void checkAuthentification() {
		if (session.contains("logged_in_landlordId") == false
				&& session.contains("logged_in_tenantId") == false
				&& session.contains("logged_in_adminId") == false)
			Welcome.index();
	}

	/**
	 * method that generates a Report and renders a view containing the current
	 * logged in landlord, the circle object and a list of residences relating
	 * to all residences within a circle search area, it uses the passed in
	 * parameters- radius to specify how big the range is and both the latcenter
	 * and lngcenter to indicate where the center or starting point is
	 * 
	 * @param double
	 *            radius
	 * @param double
	 *            latcenter
	 * @param double
	 *            lngcenter
	 */
	public static void reportResultLandlord(double radius, double latcenter,
			double lngcenter) {
		Logger.info(radius + " " + latcenter + " " + lngcenter);

		// All reported residences will fall within this circle
		Circle circle = new Circle(latcenter, lngcenter, radius);

		Landlord luser = Landlords.getCurrentLandLord();
		List<Residence> residences = new ArrayList<Residence>();

		// Fetch all residences and filter out those within circle
		List<Residence> residencesAll = Residence.findAll();

		for (Residence res : residencesAll) {
			LatLng residenceLocation = res.getGeolocation();
			if (Geodistance.inCircle(residenceLocation, circle)) {
				residences.add(res);
			}
		}
		Collections.sort(residences, new ReportPriceComparator());

		render(luser, circle, residences);
	}

	/**
	 * method that generates a Report and renders a view containing the current
	 * logged in tenant, the circle object and a list of residences relating to
	 * all vacant residences within a circle search area, it uses the passed in
	 * parameters- radius to specify how big the range is and both the latcenter
	 * and lngcenter to indicate where the center or starting point is
	 * 
	 * @param double
	 *            radius
	 * @param double
	 *            latcenter
	 * @param double
	 *            lngcenter
	 */
	public static void reportResultTenant(double radius, double latcenter,
			double lngcenter) {
		Logger.info(radius + " " + latcenter + " " + lngcenter);

		// All reported residences will fall within this circle
		Circle circle = new Circle(latcenter, lngcenter, radius);

		Tenant tUser = Tenants.getCurrentTenant();
		List<Residence> residences = new ArrayList<Residence>();

		// Fetch all residences and filter out those within circle
		List<Residence> residencesAll = Residence.findAll();

		for (Residence res : residencesAll) {
			LatLng residenceLocation = res.getGeolocation();
			if (Geodistance.inCircle(residenceLocation, circle)
					&& res.tenant == null) {
				residences.add(res);
			}
		}
		Collections.sort(residences, new ReportPriceComparator());

		render(tUser, circle, residences);
	}

	/**
	 * method that renders the administration report view, including all
	 * residences and the filter type variable
	 */
	public static void adminReport() {
		String filterType = "(all)";
		List<Residence> residences = Residence.findAll();

		render(residences, filterType);
	}

	/**
	 * method contains a switch statement allowing the administrator to filter
	 * the residences based on selected criteria(vacant, rent, type, all), uses
	 * the parameter num to specify which case to implement, using the render
	 * template method therefore eliminating the use of multiple views
	 * 
	 * @param String
	 *            num
	 */
	public static void adminReportResult(String num) {
		int number = Integer.parseInt(num);
		String filterType;
		Logger.info("name is:  " + num);

		List<Residence> allResidences = Residence.findAll();
		List<Residence> residences = new ArrayList<Residence>();

		switch (number) {

			case 1 :
				Logger.info("should be sorted something went right case 1!:  "
						+ number);
				filterType = "(vacant)";

				for (Residence res : allResidences) {
					if (res.tenant == null) {
						residences.add(res);
					}
				}
				renderTemplate("/Report/adminReport.html", residences,
						filterType);
				break;

			case 2 :

				Logger.info("something went right case 2!:  " + number);
				filterType = "(rent)";

				Logger.info("Residence amount   :  " + allResidences.size());

				Collections.sort(allResidences, new ReportPriceComparator());

				residences.addAll(allResidences);

				renderTemplate("/Report/adminReport.html", residences,
						filterType);
				break;

			case 3 :
				filterType = "(type)";

				Collections.sort(allResidences, new ReportTypeComparator());

				Logger.info("something went right case 3!:  " + number);

				residences.addAll(allResidences);
				renderTemplate("/Report/adminReport.html", residences,
						filterType);

				break;

			case 4 :

				Logger.info("something went right case 4!:  " + number);
				adminReport();
				break;

			default :
				Logger.info("should be sorted something went wrong!");
				break;

		}

	}

}
