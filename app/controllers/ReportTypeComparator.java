package controllers;

import java.util.Comparator;

import models.Residence;

public class ReportTypeComparator implements Comparator<Residence> {

	/**
	 * method overridden to satisfy the implementation of the Comparator
	 * interface, used to compare residence types and return a int value(-1, 0,
	 * 1) depending on a lower, even or higher amount comparison based on there
	 * unicode values, to be used in the built in Collections.sort(array,
	 * comparator) method
	 * 
	 * @return int -1 || 0 || 1
	 */
	@Override
	public int compare(Residence res0, Residence res1) {

		return res0.type.compareToIgnoreCase(res1.type);
	}

}
