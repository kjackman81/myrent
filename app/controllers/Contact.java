package controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import models.Landlord;
import models.Message;
import play.Logger;
import play.mvc.Controller;

public class Contact extends Controller {

	/**
	 * method used to render the contact form page, with data including Lists of
	 * users, firstNames and lastNames
	 * 
	 * @param List<Landlord>
	 *            users
	 * @param List<String>
	 *            firstNames
	 * @param List<String>
	 *            lastNames
	 */
	public static void contactForm(List<Landlord> users,
			List<String> firstNames, List<String> lastNames) {

		Logger.info("landing on the contact form page");
		render(users, firstNames, lastNames);
	}

	/**
	 * method used to provide a facility where no duplicate names are entered
	 * into the dropdown lists, converts from HashSets to ArrayLists to achieve
	 * this
	 */
	public static void contact() {

		List<Landlord> allUsers = Landlord.findAll();

		HashSet<String> firstNames = new HashSet<String>();
		HashSet<String> lastNames = new HashSet<String>();

		for (Landlord u : allUsers) {
			firstNames.add(u.firstName);
		}

		for (Landlord u : allUsers) {
			lastNames.add(u.lastName);
		}
		List<String> fNames = new ArrayList<String>(firstNames);
		List<String> lNames = new ArrayList<String>(lastNames);

		contactForm(allUsers, fNames, lNames);
	}

	/**
	 * method used to acknowledge the message sent from the user, renders the
	 * acknowledge message
	 */
	public static void contactAck() {
		Logger.info("Landed on the contact Acknowledgement page");
		render();
	}

	/**
	 * method used to store the message object with parameters sent by the user
	 * in the database
	 * 
	 * @param String
	 *            firstName
	 * @param String
	 *            lastName
	 * @param String
	 *            email
	 * @param String
	 *            messageText
	 */
	public static void formAck(String firstName, String lastName, String email,
			String messageText) {
		Logger.info("firstname: " + firstName + " lastname: " + lastName
				+ " email: " + email + " messageText: " + messageText);

		if (firstName.isEmpty() || lastName.isEmpty() || email.isEmpty()
				|| messageText.isEmpty()) {
			Logger.info("please fill in all approciate data");
			contact();
		}

		Landlord luser = Landlords.getCurrentLandLord();

		Message message = new Message(luser, messageText, email);
		message.save();

		contactAck();

	}

}
