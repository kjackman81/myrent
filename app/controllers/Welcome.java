package controllers;

import play.*;
import play.mvc.*;
import play.mvc.Scope.Session;
import java.util.*;

import models.*;

public class Welcome extends Controller {

	/**
	 * method used to render the index view of the welcome page
	 */
	public static void index() {
		Logger.info("landed at Welcome page");
		render();
	}

}
